/**
 * @File Name          : AccountController.cls
 * @Description        : 
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              : 
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 8/21/2019, 4:40:54 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/21/2019, 4:40:54 PM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
global with sharing class AccountController {
    
    @auraEnabled
    public static list<account> getAccounts()
    {
        //Venky123 changes
        return [select name,accountnumber,industry,fax from Account limit 100];
        
    }
    
    //latest changes from venky

}